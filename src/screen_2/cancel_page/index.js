import React from 'react';
import {View, Text} from 'react-native';

const CancelPage = () => {
  return (
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
      }}>
      <Text>Cancel Page</Text>
    </View>
  );
};

export default CancelPage;
