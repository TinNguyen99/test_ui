/**
 * @format
 */

import {AppRegistry} from 'react-native';
import Screen1 from './src/screen_1';
import MainScreen from './src/screen_2';

import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => MainScreen);
